# Citations

## En vrac

- « Il y a toujours dans le monde une personne qui en attend une autre. » - Paulo Coelho
- « Le plus grand voyageur est celui qui a su faire une fois le tour de lui-même. » - Confucius
- « Le moment présent a un avantage sur tous les autres : il nous appartient. » - Charles Caleb Colton
- « Il faut, avant de donner la vie, l’aimer et la faire aimer. » - Henry Bordeaux
- « Il n'y a pas de vent favorable pour celui qui ne sait pas où il va. » - Sénèque
- « Le bonheur n'est pas dans la recherche de la perfection, mais dans la tolérance de l'imperfection. » - Zen Shin
- « Le seul véritable échec est celui de ne pas essayer. » - George Clooney
- « La vie c'est comme une bicyclette, il faut avancer pour ne pas perdre l'équilibre. » - Albert Einstein
- « L'échec est le fondement de la réussite. » - Lao-Tseu
- « Les deux jours les plus importants de votre vie sont le jour où vous êtes né et le jour où vous découvrez pourquoi. » - Mark Twain
- « Un voyage de mille miles commence par un seul pas. » - Lao Tzu
- « Cela semble toujours impossible jusqu'à ce que ce soit fait. » - Nelson Mandela
- « Il est difficile d'échouer, mais il est encore pire de ne pas avoir essayé de réussir. » - Theodore Roosevelt
- « Le changement est la loi de la vie. Ceux qui ne regardent que le passé ou le présent sont certains de manquer le futur. » - John F. Kennedy

## Socrate

- « La seule vraie sagesse est dans le fait de savoir que vous ne savez rien. »
- « Je ne peux rien apprendre à personne, je ne peux que les faire réfléchir". »
- « Il est préférable d'être malheureux dans la connaissance que d'être heureux dans l'ignorance. »
- « Il n'y a qu'un seul bien, la connaissance, et un seul mal, l'ignorance. »
- « Ce que je sais, c'est que je ne sais rien. »
- « la vie sans examen ne vaut pas la peine d'être vécue. »
- « Un homme doit être vertueux pour une vie heureuse. »
- « N'est-il pas plus merveilleux de faire plutôt que d'être fait ? »
